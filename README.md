

<img src="example.png">

Example: Suppose we are searching for number: 44 , your program should return: 6

## Solution

Prerequisites: CMake, GCC/Clang, MPI (e.g. OpenMPI)

```
cd kabir
mkdir build
cd build
cmake ..
```

Testing:

```
cd build
./kabir_serial 44 100000
Searching for value = 44
Using a randomly-generated 100000 x 100000 matrix
40264
./kabir_openmp 44 1000
Searching for value = 44
Using a randomly-generated 1000 x 1000 matrix
OpenMP time = 0.005160 sec
40264
mpirun -n 100 ./kabir_mpi 44 1000
...
MPI time = 2.257767 sec
40264
```

Note the problem is not large enough for MPI implementation to be efficent, due to the matrix broadcasting interface

The serial version additionally shows debug output if n <= 15:

```
./kabir_serial 23 10
Searching for value = 23
Using a randomly-generated 10 x 10 matrix
Found match in horizontal mode at (j, i) = (2, 6):
3 6 7 5 3 5 6 2 9 1 
2 7 0 9 3 6 0 6 2 6 
1 8 7 9 2 0 _ _ 7 5 
9 2 2 8 9 7 3 6 1 2 
9 3 1 9 4 7 8 4 5 0 
3 6 1 0 6 3 2 0 6 1 
5 5 4 7 6 5 6 9 3 7 
4 5 2 5 4 7 4 4 3 0 
7 8 6 8 8 4 3 1 4 9 
2 0 6 8 9 2 6 6 4 9 
Found match in vertical mode at (j, i) = (2, 6):
3 6 7 5 3 5 6 2 9 1 
2 7 0 9 3 6 0 6 2 6 
1 8 7 9 2 0 _ 3 7 5 
9 2 2 8 9 7 _ 6 1 2 
9 3 1 9 4 7 8 4 5 0 
3 6 1 0 6 3 2 0 6 1 
5 5 4 7 6 5 6 9 3 7 
4 5 2 5 4 7 4 4 3 0 
7 8 6 8 8 4 3 1 4 9 
2 0 6 8 9 2 6 6 4 9 
Found match in vertical mode at (j, i) = (3, 1):
3 6 7 5 3 5 6 2 9 1 
2 7 0 9 3 6 0 6 2 6 
1 8 7 9 2 0 2 3 7 5 
9 _ 2 8 9 7 3 6 1 2 
9 _ 1 9 4 7 8 4 5 0 
3 6 1 0 6 3 2 0 6 1 
5 5 4 7 6 5 6 9 3 7 
4 5 2 5 4 7 4 4 3 0 
7 8 6 8 8 4 3 1 4 9 
2 0 6 8 9 2 6 6 4 9 
Found match in adiagonal mode at (j, i) = (9, 5):
3 6 7 5 3 5 6 2 9 1 
2 7 0 9 3 6 0 6 2 6 
1 8 7 9 2 0 2 3 7 5 
9 2 2 8 9 7 3 6 1 2 
9 3 1 9 4 7 8 4 5 0 
3 6 1 0 6 3 2 0 6 1 
5 5 4 7 6 5 6 9 3 7 
4 5 2 5 4 7 4 4 3 0 
7 8 6 8 8 4 _ 1 4 9 
2 0 6 8 9 _ 6 6 4 9 
4
```
