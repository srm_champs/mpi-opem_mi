#include <omp.h>
#include <stdio.h>

void process_matrix(int* argc, char*** argv, int n, unsigned char m[n + 1][n + 1], unsigned char tgt)
{
	double start = omp_get_wtime();

	int result = 0;
	#pragma omp parallel for collapse(2) reduction(+:result)
	for (int j = 0; j < n; j++)
		for (int i = 0; i < n; i++)
		{
			unsigned short val = (unsigned char)100;

			val = 10 * m[j][i] + m[j][i + 1];
			if (val == tgt) result++;

			val = 10 * m[j][i] + m[j + 1][i];
			if (val == tgt) result++;

			val = 10 * m[j][i] + m[j + 1][i + 1];
			if (val == tgt) result++;

			val = 10 * m[j + 1][i] + m[j][i + 1];
			if (val == tgt) result++;
		}

	double finish = omp_get_wtime();

	printf("OpenMP time = %f sec\n", finish - start);
	printf("%d\n", result);
}

