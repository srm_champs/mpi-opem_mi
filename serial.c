#include <stdio.h>

#if 0
#define report_result(...)
#else
static void report_result(int n, unsigned char m[n + 1][n + 1], unsigned char tgt, const char* mode, int j0, int i0, int j1, int i1)
{
	if (n > 15) return;
	
	printf("Found match in %s mode at (j, i) = (%d, %d):\n", mode, j0, i0);
	for (int j = 0; j < n; j++)
        {
                for (int i = 0; i < n; i++)
		{
			if ((j == j0) && (i == i0))
				printf("_ ");
			else if ((j == j1) && (i == i1))
				printf("_ ");
			else
				printf("%d ", (int)m[j][i]);
		}
		printf("\n");
	}
}
#endif

void process_matrix(int* argc, char*** argv, int n, unsigned char m[n + 1][n + 1], unsigned char tgt)
{
	int result = 0;
	for (int j = 0; j < n; j++)
	{
		for (int i = 0; i < n; i++)
		{
			unsigned short val = (unsigned char)100;

			val = 10 * m[j][i] + m[j][i + 1];
			if (val == tgt)
			{
				report_result(n, m, tgt, "horizontal", j, i, j, i + 1);
				result++;
			}

			val = 10 * m[j][i] + m[j + 1][i];
			if (val == tgt)
			{
				report_result(n, m, tgt, "vertical", j, i, j + 1, i);
				result++;
			}

			val = 10 * m[j][i] + m[j + 1][i + 1];
			if (val == tgt)
			{
				report_result(n, m, tgt, "diagonal", j, i, j + 1, i + 1);
				result++;
			}

			val = 10 * m[j + 1][i] + m[j][i + 1];
			if (val == tgt)
			{
				report_result(n, m, tgt, "adiagonal", j + 1, i, j, i + 1);
				result++;
			}
		}
	}

	printf("%d\n", result);
}

