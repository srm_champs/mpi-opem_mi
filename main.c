#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void process_matrix(int* argc, char*** argv, int n, unsigned char matrix[n + 1][n + 1], unsigned char target);

static unsigned char* create_matrix(int n)
{
	// In our implementation, we add some redundancy in order to
	// get rid of excessive conditioning. That is, instead of
	// handling last row and last column as special cases,
	// we create a matrix with 1 extra row and 1 extra column
	// and fill the extra elements with impossible values, which
	// can never score into the final result.

	unsigned char* matrix = (unsigned char*)malloc((n + 1) * (n + 1) * sizeof(matrix[0]));
	memset(matrix, (unsigned char)100, (n + 1) * (n + 1) * sizeof(matrix[0]));

	return matrix;
}

static void free_matrix(int n, unsigned char* matrix)
{
	free(matrix);
}

static unsigned char* default_matrix(int* n_)
{
#if 0
	static char matrix_[] =
	{
		0, 4,
		4, 0
	};
#else
	static char matrix_[] =
	{
		4, 8, 3, 6, 2, 1, 9, 3, 5, 2, 9, 6, 4, 9, 1,
		6, 5, 8, 1, 4, 3, 3, 1, 4, 1, 4, 8, 6, 7, 7,
		2, 3, 6, 7, 1, 0, 6, 7, 0, 4, 1, 3, 0, 6, 8,
		8, 7, 2, 6, 2, 5, 8, 1, 2, 7, 0, 2, 7, 7, 6,
		7, 8, 1, 8, 7, 0, 4, 5, 6, 9, 5, 9, 3, 2, 0,
		3, 0, 1, 5, 1, 5, 4, 7, 0, 7, 2, 8, 5, 6, 6,
		2, 6, 3, 5, 0, 8, 5, 1, 6, 4, 5, 1, 3, 7, 1,
		7, 1, 0, 3, 7, 0, 2, 4, 4, 6, 7, 4, 1, 2, 9,
		1, 9, 5, 4, 5, 3, 7, 1, 2, 1, 4, 9, 3, 0, 3,
		2, 1, 3, 8, 7, 0, 9, 0, 0, 8, 1, 8, 2, 3, 9,
		9, 7, 5, 2, 9, 2, 9, 8, 6, 6, 1, 9, 1, 8, 7,
		1, 3, 7, 9, 1, 0, 5, 9, 5, 0, 3, 1, 9, 4, 3,
		3, 2, 1, 1, 2, 6, 7, 6, 6, 3, 1, 0, 1, 4, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	};
#endif

	int n = sqrt(sizeof(matrix_) / sizeof(matrix_[0]));

	unsigned char (*matrix)[n + 1] = (unsigned char(*)[n + 1])create_matrix(n);

	for (int j = 0; j < n; j++)
		for (int i = 0; i < n; i++)
			matrix[j][i] = matrix_[j * n + i];

	*n_ = n;

	return (unsigned char*)matrix;
}	

static unsigned char* generate_matrix(int n)
{
	unsigned char (*matrix)[n + 1] = (unsigned char(*)[n + 1])create_matrix(n);

        for (int j = 0; j < n; j++)
                for (int i = 0; i < n; i++)
			matrix[j][i] = rand() % 10;

	return (unsigned char*)matrix;
}

int main(int argc, char* argv[])
{
	int tgt = 0;
	int n = 0;
	unsigned char* matrix = NULL;

	if ((argc < 2) || (argc > 3))
	{
		printf("Usage: %s <search_value> [<matrix_size>]\n", argv[0]);
		return 1;
	}

	tgt = atoi(argv[1]);
	if ((tgt > 99) || (tgt < 0))
	{
		fprintf(stderr, "Invalid value %d, must be 0..99\n", tgt);
		return -1;
	}

	printf("Searching for value = %d\n", tgt);

	if (argc == 2)
	{
		matrix = default_matrix(&n);
		printf("Using the problem matrix %d x %d from the assignment\n", n, n);
	}
	else
	{
		n = atoi(argv[2]);
		matrix = generate_matrix(n);
		printf("Using a randomly-generated %d x %d matrix\n", n, n);
	}

	process_matrix(&argc, &argv, n, (unsigned char (*)[n + 1])matrix, (unsigned char)tgt);

	free_matrix(n, matrix);

	return 0;
}

