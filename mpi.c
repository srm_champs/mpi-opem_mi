#include <mpi.h>
#include <stdio.h>
#include <limits.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#ifndef HOST_NAME_MAX
# if defined(_POSIX_HOST_NAME_MAX)
#  define HOST_NAME_MAX _POSIX_HOST_NAME_MAX
# elif defined(MAXHOSTNAMELEN)
#  define HOST_NAME_MAX MAXHOSTNAMELEN
# endif
#endif /* HOST_NAME_MAX */

#define MPI_ERR_CHECK(call)                                       \
    do { int err = call;                                          \
    if (err != MPI_SUCCESS) {                                     \
        char hostname[HOST_NAME_MAX] = "";                        \
        gethostname(hostname, HOST_NAME_MAX);                     \
        char errstr[MPI_MAX_ERROR_STRING];                        \
        int szerrstr;                                             \
        MPI_Error_string(err, errstr, &szerrstr);                 \
        fprintf(stderr, "MPI error on %s at %s:%i : %s\n",        \
            hostname, __FILE__, __LINE__, errstr);                \
        MPI_Abort(MPI_COMM_WORLD, err);                           \
    }} while (0)

void process_matrix(int* argc, char*** argv, int n, unsigned char m[n + 1][n + 1], unsigned char tgt)
{
	double start = MPI_Wtime();

	MPI_ERR_CHECK(MPI_Init(argc, argv));

	int nranks, rank;
	MPI_ERR_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &nranks));
	MPI_ERR_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));

	// Broadcast the matrix from rank 0 to all other ranks,
	// as the random seed may be in sync.
	MPI_ERR_CHECK(MPI_Bcast(m, (n + 1) * (n + 1) * sizeof(m[0][0]), MPI_BYTE, 0, MPI_COMM_WORLD));

	// Partition by rows, that is every rank processes a subset
	// of matrix rows.
	int rows_per_rank = n / nranks;
	if (n % nranks) rows_per_rank++;

	int result = 0;
	for (int j = rank * rows_per_rank; (j < (rank + 1) * rows_per_rank) && (j < n); j++)
		for (int i = 0; i < n; i++)
		{
			unsigned short val = (unsigned char)100;

			val = 10 * m[j][i] + m[j][i + 1];
			if (val == tgt) result++;

			val = 10 * m[j][i] + m[j + 1][i];
			if (val == tgt) result++;

			val = 10 * m[j][i] + m[j + 1][i + 1];
			if (val == tgt) result++;

			val = 10 * m[j + 1][i] + m[j][i + 1];
			if (val == tgt) result++;
		}

	// Reduce the final result.
	int result_sum = 0;
	MPI_ERR_CHECK(MPI_Reduce(&result, &result_sum, 1, MPI_INTEGER, MPI_SUM, 0, MPI_COMM_WORLD));

	double finish = MPI_Wtime();

	if (rank == 0)
	{
		printf("MPI time = %f sec\n", finish - start);
		printf("%d\n", result_sum);
	}

	MPI_ERR_CHECK(MPI_Finalize());
}

